package pl.zgora.uz.changedetector.domain;

import pl.zgora.uz.changedetector.domain.model.vo.MessageStatus;

import java.util.List;

public interface MessagesClient {

    MessageStatus sendMessage(final String email,
                              final String title,
                              final String pageUrl,
                              final List<String> lines) throws OperationException;
}
