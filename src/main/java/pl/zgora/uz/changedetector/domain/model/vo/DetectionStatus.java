package pl.zgora.uz.changedetector.domain.model.vo;

public enum DetectionStatus {

    FOUND,
    NOT_FOUND,
    ERROR,
    UNAVAILABLE;
}
