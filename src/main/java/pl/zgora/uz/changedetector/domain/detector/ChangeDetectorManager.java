package pl.zgora.uz.changedetector.domain.detector;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.zgora.uz.changedetector.domain.*;
import pl.zgora.uz.changedetector.domain.model.PageDetails;
import pl.zgora.uz.changedetector.domain.model.vo.DetectionStatus;
import pl.zgora.uz.changedetector.port.adapter.rest.PageContentExtractor;

import java.util.List;
import java.util.UUID;

@Component
@AllArgsConstructor
public class ChangeDetectorManager {

    private final PageDetailsRepository pageDetailsRepository;
    private final PageContentExtractor pageContentExtractor;
    private final UserDetailsRepository userDetailsRepository;
    private final MessagesClient messagesClient;

    public void checkChanges() {
        final List<PageDetails> availableForDetection = pageDetailsRepository.findAvailableForDetection();
        availableForDetection.forEach(pageDetails -> {
            final DetectionStatus detectionStatus = sendMessageIfChangesWereFound(pageDetails);
            pageDetails.updateDetectionStatus(detectionStatus);
            pageDetailsRepository.save(pageDetails);
        });
    }

    private DetectionStatus sendMessageIfChangesWereFound(final PageDetails pageDetails) {
        try {
            final String url = pageDetails.getUrl();
            final List<String> keywords = pageDetails.getKeywords();
            final List<String> foundLines = pageContentExtractor.findKeywords(url, keywords);
            if (!foundLines.isEmpty()) {
                final UUID userId = pageDetails.getUserId();
                final String userEmail = userDetailsRepository.getUserEmail(userId);
                final String title = pageDetails.getTitle();
                messagesClient.sendMessage(userEmail, title, url, foundLines);
                return DetectionStatus.FOUND;
            }
            return DetectionStatus.NOT_FOUND;
        } catch (final OperationException | UserNotFoundException e) {
            return DetectionStatus.ERROR;
        }
    }
}
