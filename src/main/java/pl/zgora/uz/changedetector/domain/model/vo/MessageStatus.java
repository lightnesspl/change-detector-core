package pl.zgora.uz.changedetector.domain.model.vo;

public enum MessageStatus {

    SENT, NOT_SENT, ERROR, UNDEFINED;
}
