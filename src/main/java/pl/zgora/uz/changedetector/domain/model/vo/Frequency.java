package pl.zgora.uz.changedetector.domain.model.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Frequency {

    WEEKLY(10080),
    DAILY(1440),
    HOURLY(60),
    EVERY_TEN_MINUTES(10),
    EVERY_ONE_MINUTE(1);

    private final long minutes;
}
