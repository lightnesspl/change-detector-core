package pl.zgora.uz.changedetector.domain;

import java.util.UUID;

public interface UserDetailsRepository {

    UUID getCurrentUserId();
    String getUserEmail(final UUID id) throws UserNotFoundException;
}
