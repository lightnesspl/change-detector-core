package pl.zgora.uz.changedetector.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.zgora.uz.changedetector.domain.StringListConverter;
import pl.zgora.uz.changedetector.domain.model.vo.DetectionStatus;
import pl.zgora.uz.changedetector.domain.model.vo.Frequency;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "page_details")
public class PageDetails {

    @Id
    private UUID id;

    private UUID userId;

    private String title;

    private String url;

    @Enumerated(EnumType.STRING)
    private Frequency frequency;

    @Convert(converter = StringListConverter.class)
    private List<String> keywords;

    @Column(columnDefinition = "timestamp with time zone")
    private OffsetDateTime nextDetection;

    @Enumerated(EnumType.STRING)
    private DetectionStatus detectionStatus;

    public PageDetails(final UUID userId,
                       final String title,
                       final String url,
                       final Frequency frequency,
                       final List<String> keywords) {
        this.id = UUID.randomUUID();
        this.userId = userId;
        this.title = title;
        this.url = url;
        this.frequency = frequency;
        this.keywords = keywords;
        this.nextDetection = calculateNextDetection();
        this.detectionStatus = DetectionStatus.UNAVAILABLE;
    }

    public void updateDetectionStatus(final DetectionStatus detectionStatus) {
        this.detectionStatus = detectionStatus;
        this.nextDetection = calculateNextDetection();
    }

    public void update(final String title, final String url, final Frequency frequency, final List<String> keywords) {
        this.title = title;
        this.detectionStatus = DetectionStatus.UNAVAILABLE;
        this.url = url;
        this.frequency = frequency;
        this.keywords = keywords;
    }

    private OffsetDateTime calculateNextDetection() {
        final long minutes = frequency.getMinutes();
        return OffsetDateTime.now(ZoneOffset.UTC)
                             .plusMinutes(minutes);
    }
}
