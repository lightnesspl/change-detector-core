package pl.zgora.uz.changedetector.domain;

public class OperationException extends Exception {

    public OperationException(final Throwable cause) {
        super(cause);
    }
}
