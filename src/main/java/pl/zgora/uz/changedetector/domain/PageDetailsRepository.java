package pl.zgora.uz.changedetector.domain;

import pl.zgora.uz.changedetector.domain.model.PageDetails;

import java.util.List;
import java.util.UUID;

public interface PageDetailsRepository {

    PageDetails save(final PageDetails pageDetails);

    List<PageDetails> findAll(UUID userId);

    List<PageDetails> findAvailableForDetection();

    PageDetails find(final UUID id, final UUID userId) throws PageDetailsNotFoundException;

    void remove(final UUID id, final UUID userId) throws PageDetailsNotFoundException;
}
