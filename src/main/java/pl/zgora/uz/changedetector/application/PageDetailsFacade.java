package pl.zgora.uz.changedetector.application;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.zgora.uz.changedetector.domain.PageDetailsNotFoundException;
import pl.zgora.uz.changedetector.domain.PageDetailsRepository;
import pl.zgora.uz.changedetector.domain.UserDetailsRepository;
import pl.zgora.uz.changedetector.domain.model.PageDetails;
import pl.zgora.uz.changedetector.domain.model.vo.Frequency;
import pl.zgora.uz.changedetector.port.adapter.rest.model.body.PageDetailsBody;
import pl.zgora.uz.changedetector.port.adapter.rest.model.body.PageDetailsEditBody;

import java.util.List;
import java.util.UUID;

@Component
@AllArgsConstructor
@Transactional(rollbackFor = Throwable.class)
public class PageDetailsFacade {

    private final PageDetailsRepository pageDetailsRepository;
    private final UserDetailsRepository userDetailsRepository;

    //TODO: Może command?
    public PageDetails create(final PageDetailsBody pageDetailsBody) {
        final UUID userId = userDetailsRepository.getCurrentUserId();
        final String title = pageDetailsBody.getTitle();
        final String url = pageDetailsBody.getUrl();
        final Frequency frequency = pageDetailsBody.getFrequency();
        final List<String> keywords = pageDetailsBody.getKeywords();
        return pageDetailsRepository.save(new PageDetails(userId, title, url, frequency, keywords));
    }

    public PageDetails update(final UUID id,
                              final PageDetailsEditBody pageDetailsEditBody) throws PageDetailsNotFoundException {
        final UUID userId = userDetailsRepository.getCurrentUserId();
        final PageDetails pageDetails = pageDetailsRepository.find(id, userId);
        pageDetails.update(pageDetailsEditBody.getTitle(),
                           pageDetailsEditBody.getUrl(),
                           pageDetailsEditBody.getFrequency(),
                           pageDetailsEditBody.getKeywords());
        return pageDetailsRepository.save(pageDetails);
    }

    public void delete(final UUID id) throws PageDetailsNotFoundException {
        final UUID userId = userDetailsRepository.getCurrentUserId();
        pageDetailsRepository.remove(id, userId);
    }

    //TODO: Filtrowanie poprzez FilterQuery?
    public List<PageDetails> getAll() {
        final UUID userId = userDetailsRepository.getCurrentUserId();
        return pageDetailsRepository.findAll(userId);
    }
}
