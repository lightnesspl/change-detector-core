package pl.zgora.uz.changedetector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChangeDetectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChangeDetectorApplication.class, args);
	}
}
