package pl.zgora.uz.changedetector.port.adapter.security;

import pl.zgora.uz.changedetector.port.adapter.security.model.security.User;

public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(final User user) {
        return new JwtUser(user.getId(), user.getEmail(), user.getPassword());
    }
}
