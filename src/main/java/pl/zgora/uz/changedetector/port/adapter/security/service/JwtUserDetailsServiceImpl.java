package pl.zgora.uz.changedetector.port.adapter.security.service;

import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.zgora.uz.changedetector.port.adapter.security.JwtUserFactory;
import pl.zgora.uz.changedetector.port.adapter.security.model.security.User;
import pl.zgora.uz.changedetector.port.adapter.security.repository.UserRepository;

import java.util.Optional;

@Service
@AllArgsConstructor
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final Optional<User> userOptional = userRepository.findByEmail(username);
        if (!userOptional.isPresent()) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            return JwtUserFactory.create(userOptional.get());
        }
    }
}
