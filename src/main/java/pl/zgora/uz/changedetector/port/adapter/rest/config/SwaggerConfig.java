package pl.zgora.uz.changedetector.port.adapter.rest.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket unauthorizedApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("unauthorized")
                                                      .select()
                                                      .paths(excludeErrorPaths())
                                                      .paths(PathSelectors.ant("/auth"))
                                                      .build();
    }

    @Bean
    public Docket authorizedApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("authorized")
                                                      .select()
                                                      .paths(excludeErrorPaths())
                                                      .paths(excludeUnauthorizedPaths())
                                                      .build()
                                                      .globalOperationParameters(customHeaders());
    }

    private List<Parameter> customHeaders() {
        final List<Parameter> parameters = new ArrayList<>();
        parameters.add(new ParameterBuilder().name("Authorization")
                                             .modelRef(new ModelRef("string"))
                                             .parameterType("header")
                                             .required(true)
                                             .build());

        return parameters;
    }

    private Predicate<String> excludeErrorPaths() {
        return Predicates.not(PathSelectors.regex("/error"));
    }

    private Predicate<String> excludeUnauthorizedPaths() {
        return Predicates.not(PathSelectors.regex("/auth"));
    }
}
