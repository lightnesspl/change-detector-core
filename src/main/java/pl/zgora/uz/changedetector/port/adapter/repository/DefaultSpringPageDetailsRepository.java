package pl.zgora.uz.changedetector.port.adapter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zgora.uz.changedetector.domain.model.PageDetails;
import pl.zgora.uz.changedetector.domain.model.vo.DetectionStatus;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface DefaultSpringPageDetailsRepository extends JpaRepository<PageDetails, UUID> {

    List<PageDetails> findByNextDetectionIsBeforeOrDetectionStatus(final OffsetDateTime currentDate,
                                                                   final DetectionStatus detectionStatus);
    Optional<PageDetails> findByIdAndUserId(final UUID id, final UUID userId);

    List<PageDetails> findAllByUserId(final UUID userId);
}
