package pl.zgora.uz.changedetector.port.adapter.scheduler;

import lombok.AllArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class SchedulerStarter implements ApplicationListener<ApplicationReadyEvent> {

    private final ChangeDetectorScheduler changeDetectorScheduler;
    private final ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        threadPoolTaskScheduler.scheduleWithFixedDelay(changeDetectorScheduler, 15000);
    }
}
