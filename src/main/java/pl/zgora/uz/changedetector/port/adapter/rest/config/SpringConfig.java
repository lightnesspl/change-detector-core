package pl.zgora.uz.changedetector.port.adapter.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.client.RestTemplate;
import pl.zgora.uz.changedetector.domain.detector.ChangeDetectorManager;
import pl.zgora.uz.changedetector.port.adapter.scheduler.ChangeDetectorScheduler;

@Configuration
public class SpringConfig {

    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        final ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(5);
        return threadPoolTaskScheduler;
    }

    @Bean
    public ChangeDetectorScheduler changeDetectorScheduler(final ChangeDetectorManager changeDetectorManager) {
        return new ChangeDetectorScheduler(changeDetectorManager);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
