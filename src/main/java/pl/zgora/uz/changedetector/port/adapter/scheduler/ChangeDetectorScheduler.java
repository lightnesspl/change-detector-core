package pl.zgora.uz.changedetector.port.adapter.scheduler;

import lombok.AllArgsConstructor;
import pl.zgora.uz.changedetector.domain.detector.ChangeDetectorManager;

@AllArgsConstructor
public class ChangeDetectorScheduler implements Runnable {

    private final ChangeDetectorManager changeDetectorManager;

    @Override
    public void run() {
        changeDetectorManager.checkChanges();
    }
}
