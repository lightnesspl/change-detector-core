package pl.zgora.uz.changedetector.port.adapter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.zgora.uz.changedetector.port.adapter.security.model.security.User;

import java.util.Optional;
import java.util.UUID;

public interface DefaultSpringUserDetailsRepository extends JpaRepository<User, UUID> {

    @Override
    Optional<User> findById(final UUID id);
}
