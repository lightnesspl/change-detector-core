package pl.zgora.uz.changedetector.port.adapter.mail;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import pl.zgora.uz.changedetector.domain.MessagesClient;
import pl.zgora.uz.changedetector.domain.OperationException;
import pl.zgora.uz.changedetector.domain.model.vo.MessageStatus;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class MailClient implements MessagesClient {

    public JavaMailSender emailSender;
    public TemplateEngine templateEngine;

    @Override
    public MessageStatus sendMessage(final String email,
                                     final String title,
                                     final String pageUrl,
                                     final List<String> lines) throws OperationException {
        try {
            final MimeMessage mimeMessage = emailSender.createMimeMessage();
            final MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setTo(email);

            final String subject = createMailSubject(title);
            mimeMessageHelper.setSubject(subject);

            final String content = createContent(lines, pageUrl);
            mimeMessageHelper.setText(content, true);
            emailSender.send(mimeMessage);
            return MessageStatus.SENT;
        } catch (final MessagingException | MailException e) {
            log.error(e.getMessage(), e);
            throw new OperationException(e);
        }
    }

    private String createMailSubject(final String title) {
        return String.format("Changes in - %s", title);
    }

    private String createContent(final List<String> lines, final String pageUrl) {
        final Context context = new Context();
        context.setVariable("lines", lines);
        context.setVariable("url", pageUrl);
        return templateEngine.process("template", context);
    }
}
