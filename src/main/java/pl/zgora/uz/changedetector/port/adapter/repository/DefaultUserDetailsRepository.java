package pl.zgora.uz.changedetector.port.adapter.repository;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import pl.zgora.uz.changedetector.domain.UserDetailsRepository;
import pl.zgora.uz.changedetector.domain.UserNotFoundException;
import pl.zgora.uz.changedetector.port.adapter.security.JwtUser;
import pl.zgora.uz.changedetector.port.adapter.security.model.security.User;

import java.util.UUID;

@Repository
@AllArgsConstructor
public class DefaultUserDetailsRepository implements UserDetailsRepository {

    private final DefaultSpringUserDetailsRepository repository;

    @Override
    public UUID getCurrentUserId() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final JwtUser currentUser = (JwtUser) securityContext.getAuthentication()
                                                             .getPrincipal();
        return currentUser.getId();
    }

    @Override
    public String getUserEmail(final UUID id) throws UserNotFoundException {
        return repository.findById(id)
                         .map(User::getEmail)
                         .orElseThrow(UserNotFoundException::new);
    }
}
