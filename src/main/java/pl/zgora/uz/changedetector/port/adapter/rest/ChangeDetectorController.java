package pl.zgora.uz.changedetector.port.adapter.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.zgora.uz.changedetector.application.PageDetailsFacade;
import pl.zgora.uz.changedetector.domain.OperationException;
import pl.zgora.uz.changedetector.domain.PageDetailsNotFoundException;
import pl.zgora.uz.changedetector.domain.model.PageDetails;
import pl.zgora.uz.changedetector.port.adapter.rest.model.body.PageDetailsBody;
import pl.zgora.uz.changedetector.port.adapter.rest.model.body.PageDetailsEditBody;
import pl.zgora.uz.changedetector.port.adapter.rest.model.body.PagePreviewBody;
import pl.zgora.uz.changedetector.port.adapter.rest.model.response.PagePreview;

import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class ChangeDetectorController {

    private final PageContentExtractor pageContentExtractor;
    private final PageDetailsFacade pageDetailsFacade;

    @ApiOperation(value = "Create page details")
    @PostMapping(value = "/page_details", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDetails> createPageDetails(@ApiParam(required = true, name = "Page details body")
                                                         @RequestBody final PageDetailsBody pageDetailsBody) {
        final PageDetails pageDetails = pageDetailsFacade.create(pageDetailsBody);
        return ResponseEntity.status(HttpStatus.CREATED)
                             .body(pageDetails);
    }

    @ApiOperation(value = "Update page details")
    @PutMapping(value = "/page_details/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDetails> updatePageDetails(@ApiParam(required = true, name = "Id")
                                                         @PathVariable final UUID id,
                                                         @ApiParam(required = true, name = "Page details edit body")
                                                         @RequestBody final PageDetailsEditBody pageDetailsEditBody)
            throws PageDetailsNotFoundException {
        final PageDetails pageDetails = pageDetailsFacade.update(id, pageDetailsEditBody);
        return ResponseEntity.status(HttpStatus.OK)
                             .body(pageDetails);
    }

    @ApiOperation(value = "Delete page details")
    @DeleteMapping(value = "/page_details/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updatePageDetails(@ApiParam(required = true, name = "Id")
                                            @PathVariable final UUID id)
            throws PageDetailsNotFoundException {
        pageDetailsFacade.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT)
                             .build();
    }

    @ApiOperation(value = "Get all created page details")
    @GetMapping(value = "/page_details", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PageDetails>> getAllPageDetails() {
        final List<PageDetails> pageDetails = pageDetailsFacade.getAll();
        return ResponseEntity.status(HttpStatus.OK)
                             .body(pageDetails);
    }

    @ApiOperation(value = "Page Preview")
    @PostMapping(value = "/page_preview", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<PagePreview> getPagePreview(@ApiParam(required = true, name = "Page preview body")
                                                      @RequestBody final PagePreviewBody pagePreviewBody) throws OperationException {
        final String content = pageContentExtractor.extract(pagePreviewBody.getUrl());
        return ResponseEntity.ok(new PagePreview(content));
    }
}
