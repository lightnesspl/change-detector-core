package pl.zgora.uz.changedetector.port.adapter.rest.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PagePreview {

    private String content;
}
