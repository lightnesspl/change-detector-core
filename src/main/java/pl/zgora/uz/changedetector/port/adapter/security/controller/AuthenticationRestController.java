package pl.zgora.uz.changedetector.port.adapter.security.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.zgora.uz.changedetector.port.adapter.security.JwtAuthenticationRequest;
import pl.zgora.uz.changedetector.port.adapter.security.JwtTokenUtil;
import pl.zgora.uz.changedetector.port.adapter.security.JwtUser;
import pl.zgora.uz.changedetector.port.adapter.security.service.JwtAuthenticationResponse;

@RestController
public class AuthenticationRestController {

    private final AuthenticationManager authenticationManager;

    private final JwtTokenUtil jwtTokenUtil;

    public AuthenticationRestController(final AuthenticationManager authenticationManager,
                                        final JwtTokenUtil jwtTokenUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @RequestMapping(value = "${jwt.route.authentication.path}", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody final JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {

        final UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                authenticationRequest.getUsername(),
                authenticationRequest.getPassword());
        final Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        final JwtUser jwtUser = (JwtUser) authentication.getPrincipal();
        final String token = jwtTokenUtil.generateToken(jwtUser);

        return ResponseEntity.ok(new JwtAuthenticationResponse(token));
    }
}
