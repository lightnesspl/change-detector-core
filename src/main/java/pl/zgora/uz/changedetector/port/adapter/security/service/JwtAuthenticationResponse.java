package pl.zgora.uz.changedetector.port.adapter.security.service;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class JwtAuthenticationResponse {

    private String token;

    public JwtAuthenticationResponse(final String token) {
        this.token = token;
    }
}
