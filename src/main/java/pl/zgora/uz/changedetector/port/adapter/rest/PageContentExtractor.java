package pl.zgora.uz.changedetector.port.adapter.rest;

import lombok.AllArgsConstructor;
import net.htmlparser.jericho.Source;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.zgora.uz.changedetector.domain.OperationException;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PageContentExtractor {

    private static final String MOZILLA_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36";

    private final RestTemplate restTemplate;

    String extract(final String url) throws OperationException {
        try {
            final HttpEntity<String> entity = createHttpEntity();
            final ResponseEntity<String> responseEntity = restTemplate.exchange(url,
                                                                                HttpMethod.GET,
                                                                                entity,
                                                                                String.class);

            final Source source = createSourceFromResponse(responseEntity.getBody());
            return source.getRenderer()
                         .setIncludeAlternateText(false)
                         .setIncludeHyperlinkURLs(false)
                         .toString();
        } catch(final Exception e) {
            throw new OperationException(e);
        }
    }

    public List<String> findKeywords(final String url, final List<String> keywords) throws OperationException {
        final String pageContent = extract(url);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(pageContent));
        return bufferedReader.lines()
                             .filter(line -> containsKeywords(line, keywords))
                             .map(this::format)
                             .collect(Collectors.toList());
    }

    private String format(final String line) {
        return line.replace("*", "").trim();
    }

    private boolean containsKeywords(final String line, final List<String> keywords) {
        return keywords.stream()
                       .anyMatch(keyword -> line.toLowerCase()
                                                .contains(keyword.toLowerCase()));
    }

    private Source createSourceFromResponse(final String response) {
        final Source source = new Source(response);
        source.setLogger(null);
        return source;
    }

    private HttpEntity<String> createHttpEntity() {
        final HttpHeaders httpHeaders = createHttpHeaders();
        return new HttpEntity<>(null, httpHeaders);
    }

    private HttpHeaders createHttpHeaders() {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.USER_AGENT, MOZILLA_AGENT);
        return httpHeaders;
    }
}
