package pl.zgora.uz.changedetector.port.adapter.rest.model.body;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.zgora.uz.changedetector.domain.StringListConverter;
import pl.zgora.uz.changedetector.domain.model.vo.Frequency;

import javax.persistence.Convert;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "Page Details Body")
public class PageDetailsEditBody {

    @ApiModelProperty(example = "My title")
    private String title;

    @ApiModelProperty(example = "http://abilet.pl")
    private String url;

    @Enumerated(EnumType.STRING)
    private Frequency frequency;

    @Convert(converter = StringListConverter.class)
    private List<String> keywords;
}
