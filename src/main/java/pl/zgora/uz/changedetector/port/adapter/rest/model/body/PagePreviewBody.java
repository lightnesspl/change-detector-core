package pl.zgora.uz.changedetector.port.adapter.rest.model.body;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "Page Preview Body")
public class PagePreviewBody {

    @ApiModelProperty(example = "http://abilet.pl")
    private String url;
}
