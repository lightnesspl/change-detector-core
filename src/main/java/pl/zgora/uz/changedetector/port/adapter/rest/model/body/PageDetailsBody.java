package pl.zgora.uz.changedetector.port.adapter.rest.model.body;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.zgora.uz.changedetector.domain.model.vo.Frequency;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "Page Details Body")
public class PageDetailsBody {

    @ApiModelProperty(example = "My title")
    private String title;

    @ApiModelProperty(example = "http://abilet.pl")
    private String url;

    @ApiModelProperty(example = "HOURLY")
    private Frequency frequency;

    @ApiModelProperty(dataType = "List", allowableValues = "Zastal")
    private List<String> keywords;
}
