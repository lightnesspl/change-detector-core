package pl.zgora.uz.changedetector.port.adapter.repository;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.zgora.uz.changedetector.domain.PageDetailsNotFoundException;
import pl.zgora.uz.changedetector.domain.PageDetailsRepository;
import pl.zgora.uz.changedetector.domain.model.PageDetails;
import pl.zgora.uz.changedetector.domain.model.vo.DetectionStatus;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

@Repository
@AllArgsConstructor
public class DefaultPageDetailsRepository implements PageDetailsRepository {

    private final DefaultSpringPageDetailsRepository repository;

    @Override
    public PageDetails save(final PageDetails pageDetails) {
        return repository.save(pageDetails);
    }

    @Override
    public List<PageDetails> findAll(final UUID userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    public List<PageDetails> findAvailableForDetection() {
        return repository.findByNextDetectionIsBeforeOrDetectionStatus(OffsetDateTime.now(ZoneOffset.UTC),
                                                                         DetectionStatus.UNAVAILABLE);
    }

    @Override
    public PageDetails find(final UUID id, final UUID userId) throws PageDetailsNotFoundException {
        return repository.findByIdAndUserId(id, userId).orElseThrow(PageDetailsNotFoundException::new);
    }

    @Override
    public void remove(final UUID id, final UUID userId) throws PageDetailsNotFoundException {
        final PageDetails pageDetails = find(id, userId);
        repository.delete(pageDetails);
    }
}
