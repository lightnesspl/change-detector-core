package pl.zgora.uz.changedetector.port.adapter.security.model.security;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "user_details")
@Getter
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue
    private UUID id;

    private String email;

    private String password;

    public User(final String email, final String password) {
        this.id = UUID.randomUUID();
        this.email = email;
        this.password = password;
    }
}